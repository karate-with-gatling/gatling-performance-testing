Feature: shop Products
  Background:
    * header accept = '*/*'
    * header accept-encoding = 'gzip, deflate, br'
    * header accept-language = 'en-US,en;q=0.9,ta;q=0.8'
    * header content-type = 'application/json'
    * def pause = __gatling.pause
    #   payload for Shop products1
    * def getFitmentPayload = '{"operationName":"getFitment","variables":{"assemblyId":"' + __gatling.assemblyIdNew + '","trimId":"' + __gatling.trimId + '","vehicleId":"' + __gatling.Model + '"},"query":"query getFitment($trimId: String!, $vehicleId: String!, $assemblyId: String!) {  vehicleSelection {    fitment(trimId: $trimId, vehicleId: $vehicleId, assemblyId: $assemblyId) {      vehicleAssembly {        vehicleAssemblyID        vehicle {          id          make          model          vehicleLargeImage {            url            __typename          }          vehicleSmallImage {            url            __typename          }          type          year          __typename        }        trim {          id          description          chassis {            drive {              dual              __typename            }            __typename          }          __typename        }        assembly {          id          __typename        }        isStaggered        frontTireAspectRatio        rearTireAspectRatio        frontTireSize        rearTireSize        frontTireWheelRimDiameter        rearTireWheelRimDiameter        frontWheelRimDiameter        rearWheelRimDiameter        __typename      }      __typename    }    __typename  }}"}'
    * json getFitmentPayloadNew = getFitmentPayload
  Scenario: Shop products1
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getFitmentPayloadNew
    And header karate-name = 'SC1_DTC_webOrder_10_getFitment'
    When method POST
    And match response.data.vehicleSelection.fitment.vehicleAssembly.vehicle != '[#0]'
    Then status 200
    * def frontTireSize = response.data.vehicleSelection.fitment.vehicleAssembly.frontTireSize
    * def rearTireSize = response.data.vehicleSelection.fitment.vehicleAssembly.rearTireSize
    * def rearWheelRimDiameter = response.data.vehicleSelection.fitment.vehicleAssembly.rearWheelRimDiameter
    * def frontWheelRimDiameter = response.data.vehicleSelection.fitment.vehicleAssembly.frontWheelRimDiameter
    * def vehicleAssemblyID = response.data.vehicleSelection.fitment.vehicleAssembly.vehicleAssemblyID
    * def isStaggered = response.data.vehicleSelection.fitment.vehicleAssembly.isStaggered
    * print 'Isstaggeerd values is:' + isStaggered + ' and frontTire:' + frontTireSize
    * def desNew = __gatling.description
    * pause(2000)
    # payload for Tire Size example 205/45RL 17
    * def getSuggestedSellingPayload = '{"operationName":"getSuggestedSelling","variables":{"storeCode":"' + __gatling.storeId + '","suggestionInput":{"browse":{"productSet":[],"isTire":false,"isSet":false,"isWheel":false,"isAccessory":false,"products":[]},"ignoreProducts":{"set":[],"accessories":[],"tires":[],"wheels":[]},"isAccessorySearched":false,"isTireSearched":false,"isWheelSearched":false,"longitude":' + __gatling.longitude + ',"latitude":' + __gatling.latitude + ',"pdpInfo":null,"axleType":"BOTH","page":"HOMEPAGE","treadwellRequest":null},"vehicleInfo":{"code":null,"frontTireSize":"' + frontTireSize + '","frontWheelRimDiameter":"' + frontWheelRimDiameter + '","rearTireSize":"' + rearTireSize + '","rearWheelRimDiameter":"' + rearWheelRimDiameter + '","sizeQualifier":"0","isSaved":false,"vehicleAssemblyId":"' + vehicleAssemblyID + '"}},"query":"query getSuggestedSelling($suggestionInput: SuggestionInput!, $storeCode: String!, $vehicleInfo: VehicleInput!) {  productSearch {    suggestion(storeCode: $storeCode, vehicleInfo: $vehicleInfo, suggestionInput: $suggestionInput) {      title      products {        aggregates {          itemType          __typename        }        brand        code        description        gbb        icons {          altText          __typename        }        images {          altText          format          imageType          url          __typename        }        mapPrice {          value          __typename        }        mapRuleSatisfied        mapDisplayRule {          mapPriceDisplay          __typename        }        name        price {          value          formattedValue          __typename        }        priceRange {          minPrice {            formattedValue            value            __typename          }          maxPrice {            formattedValue            value            __typename          }          __typename        }        mapPrice {          currencyIso          formattedValue          value          __typename        }        productType        rearProduct {          price {            value            __typename          }          __typename        }        unitName        url        __typename      }      suggestionAnalytics {        linkName        productCode        __typename      }      __typename    }    __typename  }}"}'
    * json getSuggestedSellingPayloadNew = getSuggestedSellingPayload
    # Click on find tire option
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getSuggestedSellingPayloadNew
    And header karate-name = 'SC1_DTC_webOrder_11_getSuggestedSelling'
    When method POST
    And match response.data.productSearch.suggestion.products[*] != '[#0]'
    Then status 200
    * pause(2000)
    # payload for Tire Image1
    * def storeDetailPagePayload = '{"operationName":"StoreByCodeStoreDetailPage","variables":{"storeCode":"' + __gatling.storeId + '"},"query":"query StoreByCodeStoreDetailPage($storeCode: String!) {  store {    byCode(storeCode: $storeCode) {      winterStore      remainingWinterDays      __typename    }    __typename  }}"}'
    * json storeDetailPagePayloadNew = storeDetailPagePayload
    # Click on Tire Image1
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request storeDetailPagePayloadNew
    And header karate-name = 'SC1_DTC_webOrder_12_StoreByCodeStoreDetailPage'
    When method POST
    And match response.data.store != '[#0]'
    Then status 200
    * pause(2000)