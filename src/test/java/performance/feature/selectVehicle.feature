Feature: Select the year for getMakes
  Background:
    * header accept = '*/*'
    * header accept-encoding = 'gzip, deflate, br'
    * header accept-language = 'en-US,en;q=0.9,ta;q=0.8'
    * header content-type = 'application/json'
    # * def sleep = function(ms){ java.lang.Thread.sleep(ms) }
    # * def pause = karate.get('__gatling.pause', sleep)
    * def pause = __gatling.pause
    #   payload for year selection
    * def getMakesPayload = '{"operationName":"getMakes","variables":{"forYear":"' + __gatling.Years + '"},"query":"query getMakes($forYear: String!) {  vehicleSelection {    makes(forYear: $forYear)    __typename  }}"}'
    * json getMakesPayloadNew = getMakesPayload

  Scenario: Select the year
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getMakesPayloadNew
    And header karate-name = 'SC1_DTC_webOrder_05_getMakes'
    When method POST
    Then status 200
    And match response.data.vehicleSelection.makes != null
    * pause(2000)

    #   Payload for Make selection example Honda
    * def getModels = '{"operationName":"getModels","variables":{"forYear":"' + __gatling.Year + '","forMake":"' + __gatling.Make + '"},"query":"query getModels($forYear: String!, $forMake: String!) {  vehicleSelection {    models(forYear: $forYear, forMake: $forMake) {      description      id      __typename    }    __typename  }}"}'
    * json getModelsPayload = getModels
    # Select Make
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getModelsPayload
    And header karate-name = 'SC1_DTC_webOrder_06_getModels'
    When method POST
    And match response.data.vehicleSelection.models != '[#0]'
    Then status 200
    * pause(2000)

    #   Payload for Model example CR-z
    * def getTrimsPayload = '{"operationName":"getTrims","variables":{"forVehicleId":"' + __gatling.Model + '"},"query":"query getTrims($forVehicleId: String!) {  vehicleSelection {    trims(forVehicleId: $forVehicleId) {      id      description      __typename    }    __typename  }}"}'
    * json getTrimsPayloadNew = getTrimsPayload
    # Get Trims
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getTrimsPayloadNew
    And header karate-name = 'SC1_DTC_webOrder_07_getTrims'
    When method POST
    And match response.vehicleSelection.trims != '#[0]'
    Then status 200
    * def TrimIdNew =  response.data.vehicleSelection.trims
    * def sizeTrimId = TrimIdNew.length
    * print 'The array of TrimId is:' + TrimIdNew + 'and the size of TrimId is:' + sizeTrimId
    * def indexTrimId = Math.floor(Math.random() * sizeTrimId)
    * def trimId =  response.data.vehicleSelection.trims[indexTrimId].id
    * print 'The selected index of TrimId is:' + indexTrimId + 'and the TrimId value is:' + trimId
    * pause(2000)
    #   payload for Trim selection example Hybrid
    * def getAssembliesPayload = '{"operationName":"getAssemblies","variables":{"forTrimId":"' + trimId + '"},"query":"query getAssemblies($forTrimId: String!) {  vehicleSelection {    assemblies(forTrimId: $forTrimId) {      id      description      __typename    }    __typename  }}"}'
    * json AssembliesPayload = getAssembliesPayload
    # Trim Selection
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request AssembliesPayload
    And header karate-name = 'SC1_DTC_webOrder_08_getAssemblies'
    When method POST
    And match response.data.vehicleSelection.assemblies != '#[0]'
    Then status 200
    * def assemblyId =  response.data.vehicleSelection.assemblies
    * def sizeAssemblyId = assemblyId.length
    * print 'The array of assemblyId is:' + assemblyId + 'and the size of assembleId is:' + sizeAssemblyId
    * def indexAssemblyId = Math.floor(Math.random() * sizeAssemblyId)
    * def assemblyIdNew =  response.data.vehicleSelection.assemblies[indexAssemblyId].id
    * def description = response.data.vehicleSelection.assemblies[indexAssemblyId].description
    * print 'The randomly selected indexOf AssemblyId is:' + indexAssemblyId + 'and the assemblyId value is:' + assemblyIdNew
    * print 'description: ', description
    * pause(2000)
    # payload for Tire Size example 205/45RL 17
    * def getOptionalSizesPayload = '{"operationName":"getOptionalSizes","variables":{"assemblyId":"' + assemblyIdNew + '","trimId":"' + trimId + '","vehicleId":"' + __gatling.Model + '"},"query":"query getOptionalSizes($trimId: String!, $vehicleId: String!, $assemblyId: String!) {  vehicleSelection {    fitment(trimId: $trimId, vehicleId: $vehicleId, assemblyId: $assemblyId) {      vehicleAssembly {        optionalStaggeredSet {          offset          size {            front            rear            __typename          }          matchQualifier          __typename        }        tireSizes {          axleType          sizeQualifier          wheelSize          optionalTireSizes {            tireSize            matchQualifier            __typename          }          __typename        }        __typename      }      __typename    }    __typename  }}"}'
    * json getOptionalSizesPayloadNew = getOptionalSizesPayload
    # Tire Size
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getOptionalSizesPayload
    And header karate-name = 'SC1_DTC_webOrder_09_getOptionalSizes'
    And match response.data.vehicleSelection.assemblies != '[#0]'
    When method POST
    Then status 200
    * pause(2000)