Feature: Select the year , Make & model
Background:
    * header accept = '*/*'
    * header accept-encoding = 'gzip, deflate, br'
    * header accept-language = 'en-US,en;q=0.9,ta;q=0.8'
    * header content-type = 'application/json'

    * def sleep = function(ms){ java.lang.Thread.sleep(ms) }
    * def pause = karate.get('__gatling.pause', sleep)

    #   Get Year, Make details
    * def Year = __gatling.Year
    * def Make = __gatling.Make
    * def Model = __gatling.Model
    * def storeId = __gatling.storeId

    * def getMakesPayload =
        """
        {"operationName":"getMakes","variables":{"forYear":"${Year}"},"query":"query getMakes($forYear: String!) {  vehicleSelection {    makes(forYear: $forYear)    __typename  }}"}
        """

    * replace getMakesPayload
    | token     | value  |
    | ${Year} | Year |


    * json getMakesPayloadNew = getMakesPayload

    Scenario: Select the year

    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getMakesPayloadNew
    When method POST
    Then status 200
    * pause(5000)
    * def getMakes1 =  response.data.vehicleSelection.makes[18]
    * print 'the value of foo is:', getMakes1

    * def payload1 =
        """
        {"operationName":"getModels","variables":{"forYear":"${Year}","forMake":"${Make}"},"query":"query getModels($forYear: String!, $forMake: String!) {  vehicleSelection {    models(forYear: $forYear, forMake: $forMake) {      description      id      __typename    }    __typename  }}"}
        """

        * replace payload1
        | token     | value  |
        | ${Year} | Year |
        | ${Make} | Make |

        * json mypayload1 = payload1

    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request mypayload1
    When method POST
    Then status 200
    * pause(5000)

    * def getTrimsPayload =
            """
            {"operationName":"getTrims","variables":{"forVehicleId":"${Model}"},"query":"query getTrims($forVehicleId: String!) {  vehicleSelection {    trims(forVehicleId: $forVehicleId) {      id      description      __typename    }    __typename  }}"}
            """

            * replace getTrimsPayload
            | token     | value  |
            | ${Model} | Model |

            * json getTrimsPayloadNew = getTrimsPayload

    # Model selection
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getTrimsPayloadNew
    When method POST
    Then status 200
    * pause(5000)

    * def TrimId =  response.data.vehicleSelection.trims
    * print 'the value of TrimId is:', TrimId
    * def sizeTrimId = TrimId.length
    * print 'the value of sizeTrimId is:', sizeTrimId
    * def indexTrimId = Math.floor(Math.random() * sizeTrimId)
    * print 'selected index: ' + indexTrimId
    * def TrimIdNew =  response.data.vehicleSelection.trims[indexTrimId].id
    * print 'TrimIdNew: ', TrimIdNew


* def getAssembliesPayload =
        """
        {"operationName":"getAssemblies","variables":{"forTrimId":"${TrimIdNew}"},"query":"query getAssemblies($forTrimId: String!) {  vehicleSelection {    assemblies(forTrimId: $forTrimId) {      id      description      __typename    }    __typename  }}"}
        """

        * replace getAssembliesPayload
        | token     | value  |
        | ${TrimIdNew} | TrimIdNew |


        * json AssembliesPayload = getAssembliesPayload


    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request AssembliesPayload
    When method POST
    Then status 200
    * pause(5000)

    * def assemblyId =  response.data.vehicleSelection.assemblies
    * print 'the value of assemblyId is:', assemblyId
    * def sizeAssemblyId = assemblyId.length
    * print 'the value of sizeAssemblyId is:', sizeAssemblyId


    * def indexAssemblyId = Math.floor(Math.random() * sizeAssemblyId)
    * print 'selected indexAssemblyId: ' + indexAssemblyId
    * def assemblyIdNew =  response.data.vehicleSelection.assemblies[indexAssemblyId].id
    * print 'assemblyIdNew: ', assemblyIdNew




    * def getOptionalSizesPayload =
            """
            {"operationName":"getOptionalSizes","variables":{"assemblyId":"${assemblyIdNew}","trimId":"${TrimIdNew}","vehicleId":"${Model}"},"query":"query getOptionalSizes($trimId: String!, $vehicleId: String!, $assemblyId: String!) {  vehicleSelection {    fitment(trimId: $trimId, vehicleId: $vehicleId, assemblyId: $assemblyId) {      vehicleAssembly {        optionalStaggeredSet {          offset          size {            front            rear            __typename          }          matchQualifier          __typename        }        tireSizes {          axleType          sizeQualifier          wheelSize          optionalTireSizes {            tireSize            matchQualifier            __typename          }          __typename        }        __typename      }      __typename    }    __typename  }}"}
            """

            * replace getOptionalSizesPayload
            | token     | value  |
            | ${assemblyIdNew} | assemblyIdNew |
            | ${TrimIdNew} | TrimIdNew |
            | ${Model} | Model |


            * json getOptionalSizesPayloadNew = getOptionalSizesPayload

    # select Size
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getOptionalSizesPayloadNew
    When method POST
    Then status 200
    * pause(5000)

    * def frontTireSize = response.data.vehicleSelection.fitment.vehicleAssembly.frontTireSize
    * def rearTireSize = response.data.vehicleSelection.fitment.vehicleAssembly.rearTireSize
    * def rearWheelRimDiameter = response.data.vehicleSelection.fitment.vehicleAssembly.rearWheelRimDiameter
    * def frontWheelRimDiameter = response.data.vehicleSelection.fitment.vehicleAssembly.frontWheelRimDiameter
    * def vehicleAssemblyID = response.data.vehicleSelection.fitment.vehicleAssembly.vehicleAssemblyID

# shop Products

* def getFitmentPayload =
            """
            {"operationName":"getFitment","variables":{"assemblyId":"${assemblyIdNew}","trimId":"${TrimIdNew}","vehicleId":"${Model}"},"query":"query getFitment($trimId: String!, $vehicleId: String!, $assemblyId: String!) {  vehicleSelection {    fitment(trimId: $trimId, vehicleId: $vehicleId, assemblyId: $assemblyId) {      vehicleAssembly {        vehicleAssemblyID        vehicle {          id          make          model          vehicleLargeImage {            url            __typename          }          vehicleSmallImage {            url            __typename          }          type          year          __typename        }        trim {          id          description          chassis {            drive {              dual              __typename            }            __typename          }          __typename        }        assembly {          id          __typename        }        isStaggered        frontTireAspectRatio        rearTireAspectRatio        frontTireSize        rearTireSize        frontTireWheelRimDiameter        rearTireWheelRimDiameter        frontWheelRimDiameter        rearWheelRimDiameter        __typename      }      __typename    }    __typename  }}"}
            """

            * replace getFitmentPayload
            | token     | value  |
            | ${assemblyIdNew} | assemblyIdNew |
            | ${TrimIdNew} | TrimIdNew |
            | ${Model} | Model |


            * json getFitmentPayloadNew = getFitmentPayload

            Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
                 And request getFitmentPayloadNew
                 When method POST
                 Then status 200
                 * pause(5000)

                 * def getSuggestedSellingPayload =
                    """
                    {"operationName":"getSuggestedSelling","variables":{"storeCode":"${storeId}","suggestionInput":{"browse":{"productSet":[],"isTire":true,"isSet":false,"isWheel":false,"isAccessory":false,"products":["${TrimIdNew}"]},"ignoreProducts":{"set":[],"accessories":[],"tires":["${TrimIdNew}"],"wheels":[]},"isAccessorySearched":false,"isTireSearched":false,"isWheelSearched":false,"longitude":"-112.027104","latitude":"40.704787","pdpInfo":null,"axleType":"BOTH","page":"HOMEPAGE","treadwellRequest":null},"vehicleInfo":{"code":null,"frontTireSize":"${frontTireSize}","frontWheelRimDiameter":"${frontWheelRimDiameter}","rearTireSize":"${rearTireSize}","rearWheelRimDiameter":"${rearWheelRimDiameter}","sizeQualifier":"0","isSaved":false,"vehicleAssemblyId":"${vehicleAssemblyID}"}},"query":"query getSuggestedSelling($suggestionInput: SuggestionInput!, $storeCode: String!, $vehicleInfo: VehicleInput!) {  productSearch {    suggestion(storeCode: $storeCode, vehicleInfo: $vehicleInfo, suggestionInput: $suggestionInput) {      title      products {        aggregates {          itemType          __typename        }        brand        code        description        gbb        icons {          altText          __typename        }        images {          altText          format          imageType          url          __typename        }        mapPrice {          value          __typename        }        mapRuleSatisfied        mapDisplayRule {          mapPriceDisplay          __typename        }        name        price {          value          formattedValue          __typename        }        priceRange {          minPrice {            formattedValue            value            __typename          }          maxPrice {            formattedValue            value            __typename          }          __typename        }        mapPrice {          currencyIso          formattedValue          value          __typename        }        productType        rearProduct {          price {            value            __typename          }          __typename        }        unitName        url        __typename      }      suggestionAnalytics {        linkName        productCode        __typename      }      __typename    }    __typename  }}"}
                    """

                    * replace getSuggestedSellingPayload
                    | token     | value  |
                    | ${storeId} | storeId |
                    | ${frontTireSize} | frontTireSize |
                    | ${rearTireSize} | rearTireSize |
                    | ${rearWheelRimDiameter} | rearWheelRimDiameter |
                    | ${frontWheelRimDiameter} | frontWheelRimDiameter |
                    | ${vehicleAssemblyID} | vehicleAssemblyID |
                    | ${TrimIdNew} | TrimIdNew |

                    * json getSuggestedSellingPayloadNew = getSuggestedSellingPayload



     Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
     And request getSuggestedSellingPayloadNew
     When method POST
     Then status 200
     * pause(5000)

