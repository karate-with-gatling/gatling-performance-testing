Feature: DiscountTire webOrder
    Background:
        * header accept = '*/*'
        * header accept-encoding = 'gzip, deflate, br'
        * header accept-language = 'en-US,en;q=0.9,ta;q=0.8'
        * header content-type = 'application/json'
        #   Get StoreId from Param
        #  * def storeId = __gatling.storeId
        # ThinkTime

        * def storePayload = '{"operationName":"StoreByCode","variables":{"storeCode":"' + __gatling.storeId + '"},"query":"query StoreByCode($storeCode: String!) {  store {    byCode(storeCode: $storeCode) {      ...myStoreFields      __typename    }    __typename  }}fragment myStoreFields on StoreData {  code  address {    country {      isocode      name      __typename    }    email    line1    line2    phone    postalCode    region {      isocodeShort      name      __typename    }    town    __typename  }  winterStore  baseStore  description  displayName  bopisTurnedOff  distance  legacyStoreCode  geoPoint {    latitude    longitude    __typename  }  rating {    rating    numberOfReviews    __typename  }  weekDays {    closed    formattedDate    dayOfWeek    __typename  }  __typename}"}'
        * json StoreByCodePayload = storePayload

        # * def sleep = function(ms){ java.lang.Thread.sleep(ms) }
        # * def pause = karate.get('__gatling.pause', sleep)

    Scenario: DiscountTire webOrder
        Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
        And request StoreByCodePayload
        And header karate-name = 'SC1_DTC_webOrder_01_storeByCOde'
        When method POST
        Then status 200
        And match response.data.store.byCode.code contains '#(__gatling.storeId)'
        * def latitude = response.data.store.byCode.geoPoint.latitude
        * def longitude = response.data.store.byCode.geoPoint.longitude
        * print 'The latitude value is:' + latitude + ' and longitude value is:' + longitude
        * def postalCode = response.data.store.byCode.address.postalCode
        * def zip = postalCode.substring(0,5)
        * print 'The postal code is: ' + postalCode + ' and zip code is:' + zip
        # * pause(1000)

    # CmsHomePage
        Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
        And request '{"operationName":"CmsHomePage","variables":{"id":"homepage","siteId":"discounttire"},"query":"query CmsHomePage($id: String!, $siteId: String) {  cms {    page(id: $id, siteId: $siteId) {      documentTitle      metaTags {        name        content        __typename      }      contentSlots {        position        name: id        cmsComponents {          ... on CmsBannerCarouselComponent {            name            slides {              link {                href                text                target                __typename              }              image {                altText                url                __typename              }              __typename            }            slideInterval            id            __typename          }          ... on CmsHtmlComponent {            html            __typename          }          ... on CmsShoppingGuideComponent {            name            id            sections {              name              link {                href                text                target                __typename              }              image {                altText                url                __typename              }              heading              info              __typename            }            __typename          }          ... on CmsShoppingCategoriesComponent {            id            categories {              link {                href                text                target                __typename              }              image {                altText                url                __typename              }              type              __typename            }            __typename          }          ... on CmsArticleListComponent {            articles {              link {                href                text                target                __typename              }              image {                altText                url                __typename              }              category              title              info              __typename            }            __typename          }          ... on CmsFeaturedArticlesListComponent {            articles {              link {                href                text                target                __typename              }              image {                altText                url                __typename              }              description              id              heading              __typename            }            __typename          }          ... on CmsFilteredPromotionsComponent {            id            promotions {              code: id              link {                href                text                target                __typename              }              image {                altText                url                __typename              }              category              __typename            }            __typename          }          __typename        }        __typename      }      source      __typename    }    __typename  }}"}'
        And header karate-name = 'SC1_DTC_webOrder_02_homepage2'
        When method POST
        Then status 200
        And match response.data.cms.page.contentSlots[1].name contains 'DEALS'
        # * pause(1000)
        Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
        And request '{"operationName":"CmsHeaderFooter","variables":{"siteId":"discounttire"},"query":"query CmsHeaderFooter($siteId: String) {  cms {    globalHeader(siteId: $siteId) {      navigationNodes {        quickLinks {          id          name          url          __typename        }        linkGroups {          id          name          groupLink {            id            name            url            __typename          }          links {            name            url            __typename          }          __typename        }        id        name        __typename      }      source      __typename    }    globalFooter(siteId: $siteId) {      copyright      messageContent      footerZone2      promoContent      primaryLinks {        name        links {          name          url          __typename        }        __typename      }      secondaryLinks {        name        url        __typename      }      socialLinks {        name        url        __typename      }      source      __typename    }    __typename  }}"}'
        And header karate-name = 'SC1_DTC_webOrder_03_CmsHeaderFooter'
        When method POST
        And match response.data.cms.globalFooter.copyright != null
        Then status 200

        # * pause(1000)

        * def servicePayload = '{"operationName":"Services","variables":{"storeCode":"' + __gatling.storeId + '"},"query":"query Services($storeCode: String!) {  services(storeCode: $storeCode) {    name    code    tooltip    __typename  }}"}'
        * json servicePayloadNew = servicePayload

    # Services
        Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
        And request servicePayloadNew
        And header karate-name = 'SC1_DTC_webOrder_04_Services'
        When method POST
        And match response.data.services[*] != '[#0]'
        Then status 200
        # * pause(1000)
        # details like latitude , longtitude and zip code to getMakes Call
        # * def paramPayload = "{ name:" + latitude + ", name1:" + longitude + ", name3:" + zip + ", name4:" + postalCode + "}"
        # * json jsonPayload = paramPayload
        # * print 'The payload sent to next request is:', jsonPayload
        # * call read('getMakes.feature') jsonPayload