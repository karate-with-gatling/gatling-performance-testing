Feature: GET Api
  Background:

    # * def sleep = function(ms){ java.lang.Thread.sleep(ms) }
    # or function(ms){ } for a no-op !
    # * def pause = karate.get('__gatling.pause', sleep)

  Scenario: GET method should return 200
    Given url 'https://reqres.in/api/users/2'
    And header karate-name = 'GET-users'
    When method GET
    Then status 200

   # * pause(1000)

    * def c_userName = response.data.first_name
    # * print 'the name is:', c_userName
    # * def paramPayload = "{ user:" + c_userName + "}"
    # * json jsonPayload = paramPayload
    # * print 'the payload is ', jsonPayload
    #* call read('anotherApi.feature') jsonPayloa

    Given url 'https://reqres.in/api/users?page=2'
    And header karate-name = 'GET-users'
    When method GET
    Then status 200