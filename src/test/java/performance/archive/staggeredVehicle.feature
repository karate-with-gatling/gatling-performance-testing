Feature: Select the year for getMakes
  Background:
    * header accept = '*/*'
    * header accept-encoding = 'gzip, deflate, br'
    * header accept-language = 'en-US,en;q=0.9,ta;q=0.8'
    * header content-type = 'application/json'
        # * def sleep = function(ms){ java.lang.Thread.sleep(ms) }
        # * def pause = karate.get('__gatling.pause', sleep)
    * def descriptionStaggered = jsonMakesPayload.token
    * print 'std vehicle des:' + descriptionStaggered
    # F 245 /40 R20 SL - R 295 /35 R20 XL
    * def frontWidth = descriptionStaggered.substring(2,5)
    * def frontAspectRatio = descriptionStaggered.substring(7,9)
    * def rearWidth = descriptionStaggered.substring(21,24)
    * def rearAspectRatio = descriptionStaggered.substring(26,28)
    * print 'The frontwidth is:' + frontWidth + 'and frontaspectRatio is:' + frontAspectRatio
    * print 'The rearWidth is:' + rearWidth + 'and rearAspectRatio is:' + rearAspectRatio
    * def getSuggestedSellingPayload = '{"operationName":"getSuggestedSelling","variables":{"storeCode":"' + __gatling.storeId + '","suggestionInput":{"browse":{"productSet":[],"isTire":false,"isSet":false,"isWheel":false,"isAccessory":false,"products":[]},"ignoreProducts":{"set":[],"accessories":[],"tires":[],"wheels":[]},"isAccessorySearched":false,"isTireSearched":false,"isWheelSearched":false,"longitude":' + __gatling.longitude + ',"latitude":' + __gatling.latitude + ',"pdpInfo":null,"axleType":"BOTH","page":"HOMEPAGE","treadwellRequest":null},"vehicleInfo":{"code":null,"frontTireSize":"' + frontTireSize + '","frontWheelRimDiameter":"' + frontWheelRimDiameter + '","rearTireSize":"' + rearTireSize + '","rearWheelRimDiameter":"' + rearWheelRimDiameter + '","sizeQualifier":"0","isSaved":false,"vehicleAssemblyId":"' + vehicleAssemblyID + '"}},"query":"query getSuggestedSelling($suggestionInput: SuggestionInput!, $storeCode: String!, $vehicleInfo: VehicleInput!) {  productSearch {    suggestion(storeCode: $storeCode, vehicleInfo: $vehicleInfo, suggestionInput: $suggestionInput) {      title      products {        aggregates {          itemType          __typename        }        brand        code        description        gbb        icons {          altText          __typename        }        images {          altText          format          imageType          url          __typename        }        mapPrice {          value          __typename        }        mapRuleSatisfied        mapDisplayRule {          mapPriceDisplay          __typename        }        name        price {          value          formattedValue          __typename        }        priceRange {          minPrice {            formattedValue            value            __typename          }          maxPrice {            formattedValue            value            __typename          }          __typename        }        mapPrice {          currencyIso          formattedValue          value          __typename        }        productType        rearProduct {          price {            value            __typename          }          __typename        }        unitName        url        __typename      }      suggestionAnalytics {        linkName        productCode        __typename      }      __typename    }    __typename  }}"}'
    * json getSuggestedSellingPayloadNew = getSuggestedSellingPayload
  Scenario: Click on find tire option
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getSuggestedSellingPayloadNew
    And header karate-name = 'SC1_DTC_webOrder_11_getSuggestedSelling'
    When method POST
    And match response.data.productSearch.suggestion.products[*] != '[#0]'
    Then status 200
    # * pause(1000)
        #   payload for Tire Image1
    * def storeDetailPagePayload = '{"operationName":"StoreByCodeStoreDetailPage","variables":{"storeCode":"' + __gatling.storeId + '"},"query":"query StoreByCodeStoreDetailPage($storeCode: String!) {  store {    byCode(storeCode: $storeCode) {      winterStore      remainingWinterDays      __typename    }    __typename  }}"}'
    * json storeDetailPagePayloadNew = storeDetailPagePayload
        # Click on Tire Image1
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request storeDetailPagePayloadNew
    And header karate-name = 'SC1_DTC_webOrder_12_StoreByCodeStoreDetailPage'
    When method POST
    And match response.data.store != '[#0]'
    Then status 200
        # * pause(1000)
     #   payload for Tire Image2
    * def FitmentTireProductSearch = '{"operationName":"FitmentTireProductSearch","variables":{"searchInput":{"pageNumber":0,"searchOption":"WINTER_TIRES","pageSize":1,"staggeredType":"SET","front":{"aspectRatio":"' + frontAspectRatio + '","diameter":"' + frontWheelRimDiameter + '","width":"' + frontWidth + '"},"rear":{"aspectRatio":"' + rearAspectRatio + '","diameter":"' + rearWheelRimDiameter + '","width":"' + rearWidth + '"}},"storeCode":"' + __gatling.storeId + '","vehicleInfo":{"isSaved":false,"vehicleAssemblyId":"' + vehicleAssemblyID + '"}},"query":"query FitmentTireProductSearch($vehicleInfo: VehicleInput!, $searchInput: StaggeredTireSearchInput!, $storeCode: String!) {  fitmentSearch(vehicleInfo: $vehicleInfo) {    staggeredTireSearch(searchInput: $searchInput, storeCode: $storeCode) {      pagination {        totalNumberOfResults        __typename      }      __typename    }    __typename  }}"}'
    * json FitmentTireProductSearchPayload = FitmentTireProductSearch
        # Tire Image2
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request FitmentTireProductSearchPayload
    And header karate-name = 'SC1_DTC_webOrder_13_FitmentTireProductSearch'
    When method POST
    And match response.data.fitmentSearch != '[#0]'
    Then status 200
    # * def productCode =  response.data.fitmentSearch.standardTireSearch.results[0].code
        # * pause(1000)
    * def getStaggeredVehicleTirePayload = '{"operationName":"getStaggeredVehicleTireCTAs","variables":{"vehicleInfo":{"code":null,"frontTireSize":"' + frontTireSize + '","frontWheelRimDiameter":"' + frontWheelRimDiameter + '","rearTireSize":"' + rearTireSize  + '","rearWheelRimDiameter":"' + rearWheelRimDiameter + '","sizeQualifier":"0","isSaved":false,"vehicleAssemblyId":"' + vehicleAssemblyID + '"},"storeCode":"' + __gatling.storeId + '","searchInput":{"front":{"aspectRatio":"' + frontAspectRatio + '","diameter":"' + frontWheelRimDiameter + '","width":"' + frontWidth + '"},"pageNumber":0,"pageSize":1,"rear":{"aspectRatio":"' + rearAspectRatio + '","diameter":"' + rearWheelRimDiameter + '","width":"' + rearWidth + '"},"searchOption":"ALL","staggeredType":"SET"}},"query":"query getStaggeredVehicleTireCTAs($vehicleInfo: VehicleInput!, $searchInput: StaggeredTireSearchInput!, $storeCode: String!) {  fitmentSearch(vehicleInfo: $vehicleInfo) {    staggeredTireSearch(searchInput: $searchInput, storeCode: $storeCode) {      facets {        code        __typename      }      results {        code        __typename      }      __typename    }    __typename  }}"}'
    * json getStaggeredVehicleTirePayloadNew = getStaggeredVehicleTirePayload
    # Click on find tire option
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getStaggeredVehicleTirePayloadNew
    And header karate-name = 'SC1_DTC_webOrder_14_getStaggeredVehicleTireCTAs'
    When method POST
    And match response.data.fitmentSearch.staggeredTireSearch.results[*] != '#[0]'
    Then status 200
      # * pause(1000)
    * def productCode =  response.data.fitmentSearch.staggeredTireSearch.results[0].code
    * print 'the value of productCode latest is:', productCode
    * def getStaggeredTilesPayload = '{"operationName":"getStaggeredTiles","variables":{"vehicleInfo":{"code":null,"frontTireSize":"' + __gatling.frontTireSize + '","frontWheelRimDiameter":"' + __gatling.frontWheelRimDiameter + '","rearTireSize":"' + __gatling.rearTireSize + '","rearWheelRimDiameter":"' + __gatling.rearWheelRimDiameter + '","sizeQualifier":"0","isSaved":false,"vehicleAssemblyId":"' + __gatling.vehicleAssemblyId + '"},"storeCode":"' + __gatling.storeId + '","searchInput":{"front":{"aspectRatio":"' + frontAspectRatio + '","diameter":"' + __gatling.frontWheelRimDiameter + '","width":"' + frontWidth + '"},"treadwell":{"drivingStyle":"EVERY_DAY","quantity":2,"milesPerYear":15,"priority1":"STOPPING_DISTANCE","priority2":"LIFE_OF_TIRE","priority3":"HANDLING","priority4":"COMFORT_AND_NOISE","isOESize":true,"zipCode":"' + __gatling.postalCode + '"},"rear":{"aspectRatio":"' + rearAspectRatio + '","diameter":"' + __gatling.rearWheelRimDiameter + '","width":"' + rearWidth + '"},"staggeredType":"SET"}},"query":"query getStaggeredTiles($vehicleInfo: VehicleInput!, $searchInput: StaggeredTilesInput!, $storeCode: String!) {  fitmentSearch(vehicleInfo: $vehicleInfo) {    staggeredTiles(searchInput: $searchInput, storeCode: $storeCode) {      ...topThreeData      __typename    }    __typename  }}fragment topThreeData on ProductTilesResult {  ruleNumber  productTiles {    tile    sequenceNumber    productData {      recommendationData {        rank        __typename      }      price {        value        __typename      }      brand      images {        altText        format        imageType        url        __typename      }      line      manufacturer      unitName      url      mapDisplayRule {        messageDisplay        mapPriceDisplay        message        __typename      }      mapPrice {        value        __typename      }      mapRuleSatisfied      rearProduct {        price {          value          __typename        }        code        mapDisplayRule {          messageDisplay          mapPriceDisplay          message          __typename        }        mapPrice {          value          __typename        }        mapRuleSatisfied        __typename      }      name      code      __typename    }    __typename  }  __typename}"}'
    # * def getStaggeredTilesPayload = '{"operationName":"getStaggeredTiles","variables":{"vehicleInfo":{"code":null,"frontTireSize":"' + frontTireSize + '","frontWheelRimDiameter":"' + frontWheelRimDiameter + '","rearTireSize":"' + rearTireSize + '","rearWheelRimDiameter":"' + rearWheelRimDiameter + '","sizeQualifier":"0","isSaved":false,"vehicleAssemblyId":"' + vehicleAssemblyId + '"},"storeCode":"' + __gatling.storeId + '","searchInput":{"front":{"aspectRatio":"' + frontAspectRatio + '","diameter":"' + frontWheelRimDiameter + '","width":"' + frontWidth + '"},"treadwell":{"drivingStyle":"EVERY_DAY","quantity":2,"milesPerYear":15,"priority1":"STOPPING_DISTANCE","priority2":"LIFE_OF_TIRE","priority3":"HANDLING","priority4":"COMFORT_AND_NOISE","isOESize":true,"zipCode":"' + __gatling.postalCode + '"},"rear":{"aspectRatio":"' + rearAspectRatio + '","diameter":"' + rearWheelRimDiameter + '","width":"' + rearWidth + '"},"staggeredType":"SET"}},"query":"query getStaggeredTiles($vehicleInfo: VehicleInput!, $searchInput: StaggeredTilesInput!, $storeCode: String!) {  fitmentSearch(vehicleInfo: $vehicleInfo) {    staggeredTiles(searchInput: $searchInput, storeCode: $storeCode) {      ...topThreeData      __typename    }    __typename  }}fragment topThreeData on ProductTilesResult {  ruleNumber  productTiles {    tile    sequenceNumber    productData {      recommendationData {        rank        __typename      }      price {        value        __typename      }      brand      images {        altText        format        imageType        url        __typename      }      line      manufacturer      unitName      url      mapDisplayRule {        messageDisplay        mapPriceDisplay        message        __typename      }      mapPrice {        value        __typename      }      mapRuleSatisfied      rearProduct {        price {          value          __typename        }        code        mapDisplayRule {          messageDisplay          mapPriceDisplay          message          __typename        }        mapPrice {          value          __typename        }        mapRuleSatisfied        __typename      }      name      code      __typename    }    __typename  }  __typename}"}'
    * json getStaggeredTilesPayloadNew = getStaggeredTilesPayload
      # Shop all tires2
    Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
    And request getStaggeredTilesPayloadNew
    And header karate-name = 'SC1_DTC_webOrder_15_getStaggeredTiles'
    When method POST
    Then status 200
      # * pause(1000)
    * def productCodeNew = "{tokenProduct:" + productCode +"}"
    * json jsonsproductCodeNew = productCodeNew
        # * def result = isStaggered == true ? karate.call('staggeredVehicle.feature', {token:" + description +"}) : karate.call('stdVehicle.feature', {token:" + description +"})
    * karate.call('findTires.feature', jsonsproductCodeNew)
