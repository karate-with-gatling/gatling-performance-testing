Feature: Select the year for getMakes
    Background:
        * header accept = '*/*'
        * header accept-encoding = 'gzip, deflate, br'
        * header accept-language = 'en-US,en;q=0.9,ta;q=0.8'
        * header content-type = 'application/json'
        # * def sleep = function(ms){ java.lang.Thread.sleep(ms) }
        # * def pause = karate.get('__gatling.pause', sleep)
        #   payload for year selection
        * def getMakesPayload = '{"operationName":"getMakes","variables":{"forYear":"' + __gatling.Years + '"},"query":"query getMakes($forYear: String!) {  vehicleSelection {    makes(forYear: $forYear)    __typename  }}"}'
        * json getMakesPayloadNew = getMakesPayload
    Scenario: Select the year
        Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
        And request getMakesPayloadNew
        And header karate-name = 'SC1_DTC_webOrder_05_getMakes'
        When method POST
        Then status 200
        And match response.data.vehicleSelection.makes != null
        # * pause(1000)
        #   Payload for Make selection example Honda
        * def getModels = '{"operationName":"getModels","variables":{"forYear":"' + __gatling.Year + '","forMake":"' + __gatling.Make + '"},"query":"query getModels($forYear: String!, $forMake: String!) {  vehicleSelection {    models(forYear: $forYear, forMake: $forMake) {      description      id      __typename    }    __typename  }}"}'
        * json getModelsPayload = getModels
        #   Select Make
        Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
        And request getModelsPayload
        And header karate-name = 'SC1_DTC_webOrder_06_getModels'
        When method POST
        And match response.data.vehicleSelection.models != '[#0]'
        Then status 200
        # * pause(1000)
        #   Payload for Model example CR-z
        * def getTrimsPayload = '{"operationName":"getTrims","variables":{"forVehicleId":"' + __gatling.Model + '"},"query":"query getTrims($forVehicleId: String!) {  vehicleSelection {    trims(forVehicleId: $forVehicleId) {      id      description      __typename    }    __typename  }}"}'
        * json getTrimsPayloadNew = getTrimsPayload
        # get Trims
        Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
        And request getTrimsPayloadNew
        And header karate-name = 'SC1_DTC_webOrder_07_getTrims'
        When method POST
        And match response.vehicleSelection.trims != '#[0]'
        Then status 200
        # * pause(1000)
        * def TrimIdNew =  response.data.vehicleSelection.trims
        * def sizeTrimId = TrimIdNew.length
        * print 'The array of TrimId is:' + TrimIdNew + 'and the size of TrimId is:' + sizeTrimId
        * def indexTrimId = Math.floor(Math.random() * sizeTrimId)
        * def trimId =  response.data.vehicleSelection.trims[indexTrimId].id
        * print 'The selected index of TrimId is:' + indexTrimId + 'and the TrimId value is:' + trimId
        #   payload for Trim selection example Hybrid
        * def getAssembliesPayload = '{"operationName":"getAssemblies","variables":{"forTrimId":"' + trimId + '"},"query":"query getAssemblies($forTrimId: String!) {  vehicleSelection {    assemblies(forTrimId: $forTrimId) {      id      description      __typename    }    __typename  }}"}'
        * json AssembliesPayload = getAssembliesPayload
        # Trim Selection
        Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
        And request AssembliesPayload
        And header karate-name = 'SC1_DTC_webOrder_08_getAssemblies'
        When method POST
        And match response.data.vehicleSelection.assemblies != '#[0]'
        Then status 200
        # * pause(1000)
        * def assemblyId =  response.data.vehicleSelection.assemblies
        * def sizeAssemblyId = assemblyId.length
        * print 'The array of assemblyId is:' + assemblyId + 'and the size of assembleId is:' + sizeAssemblyId
        * def indexAssemblyId = Math.floor(Math.random() * sizeAssemblyId)
        * def assemblyIdNew =  response.data.vehicleSelection.assemblies[indexAssemblyId].id
        * def description = response.data.vehicleSelection.assemblies[indexAssemblyId].description
        * print 'The randomly selected indexOf AssemblyId is:' + indexAssemblyId + 'and the assemblyId value is:' + assemblyIdNew
        * print 'description: ', description
        # * def width = description.substring(0,3)
        # * def aspectRatio = description.substring(5,8)
        # * print 'The width is:' + width + 'and aspectRatio is:' + aspectRatio
        # payload for Tire Size example 205/45RL 17
        * def getOptionalSizesPayload = '{"operationName":"getOptionalSizes","variables":{"assemblyId":"' + __gatling.assemblyIdNew + '","trimId":"' + __gatling.trimId + '","vehicleId":"' + __gatling.Model + '"},"query":"query getOptionalSizes($trimId: String!, $vehicleId: String!, $assemblyId: String!) {  vehicleSelection {    fitment(trimId: $trimId, vehicleId: $vehicleId, assemblyId: $assemblyId) {      vehicleAssembly {        optionalStaggeredSet {          offset          size {            front            rear            __typename          }          matchQualifier          __typename        }        tireSizes {          axleType          sizeQualifier          wheelSize          optionalTireSizes {            tireSize            matchQualifier            __typename          }          __typename        }        __typename      }      __typename    }    __typename  }}"}'
        * json getOptionalSizesPayloadNew = getOptionalSizesPayload
        # Tire Size
        Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
        And request getOptionalSizesPayload
        And header karate-name = 'SC1_DTC_webOrder_09_getOptionalSizes'
        And match response.data.vehicleSelection.assemblies != '[#0]'
        When method POST
        Then status 200
        # * pause(1000)
        #   payload for Shop products1 - getFitment select - 205/45RL 17
        * def getFitmentPayload = '{"operationName":"getFitment","variables":{"assemblyId":"' + assemblyIdNew + '","trimId":"' + trimId + '","vehicleId":"' + __gatling.Model + '"},"query":"query getFitment($trimId: String!, $vehicleId: String!, $assemblyId: String!) {  vehicleSelection {    fitment(trimId: $trimId, vehicleId: $vehicleId, assemblyId: $assemblyId) {      vehicleAssembly {        vehicleAssemblyID        vehicle {          id          make          model          vehicleLargeImage {            url            __typename          }          vehicleSmallImage {            url            __typename          }          type          year          __typename        }        trim {          id          description          chassis {            drive {              dual              __typename            }            __typename          }          __typename        }        assembly {          id          __typename        }        isStaggered        frontTireAspectRatio        rearTireAspectRatio        frontTireSize        rearTireSize        frontTireWheelRimDiameter        rearTireWheelRimDiameter        frontWheelRimDiameter        rearWheelRimDiameter        __typename      }      __typename    }    __typename  }}"}'
        * json getFitmentPayloadNew = getFitmentPayload
        # Shop products
        Given url 'https://data-stg.discounttire.com/webapi/discounttire.graph'
        And request getFitmentPayloadNew
        And header karate-name = 'SC1_DTC_webOrder_10_getFitment'
        When method POST
        And match response.data.vehicleSelection.fitment.vehicleAssembly.vehicle != '[#0]'
        Then status 200
        # * pause(1000)
        * def frontTireSize = response.data.vehicleSelection.fitment.vehicleAssembly.frontTireSize
        * def rearTireSize = response.data.vehicleSelection.fitment.vehicleAssembly.rearTireSize
        * def rearWheelRimDiameter = response.data.vehicleSelection.fitment.vehicleAssembly.rearWheelRimDiameter
        * def frontWheelRimDiameter = response.data.vehicleSelection.fitment.vehicleAssembly.frontWheelRimDiameter
        * def vehicleAssemblyID = response.data.vehicleSelection.fitment.vehicleAssembly.vehicleAssemblyID
        * def isStaggered = response.data.vehicleSelection.fitment.vehicleAssembly.isStaggered
        * def value1 = "{token:" + description +"}"
        * json jsonMakesPayload = value1
        # * def result = isStaggered == true ? karate.call('staggeredVehicle.feature', {token:" + description +"}) : karate.call('stdVehicle.feature', {token:" + description +"})
        * def result = isStaggered == true ? karate.call('staggeredVehicle.feature', jsonMakesPayload) : karate.call('stdVehicle.feature', jsonMakesPayload)