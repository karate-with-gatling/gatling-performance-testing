package performance.simulations

import com.intuit.karate.gatling.PreDef._
import io.gatling.core.Predef._


class UserSimulation extends Simulation{

  val csvFeeder = csv("storeId.csv").circular
  val csvFeederVehicle = csv("vehicleDetail.csv").circular

  val protocol = karateProtocol()
  protocol.nameResolver = (req, ctx) => req.getHeader("karate-name")


  val scn = scenario(scenarioName = "DiscountTire webOrder")
    .feed(csvFeeder)
    .feed(csvFeederVehicle)
    .group("Homepage") {
      exec(karateFeature(name = "classpath:performance/feature/homePages.feature"))
    }
    .randomSwitch(
      20.0 -> exec(
        exec(session => session.markAsFailed)
          .exitHereIfFailed
      ),
      80.0 -> exec()
    )
    .group("selectVehicle") {
      exec(karateFeature(name = "classpath:performance/feature/selectVehicle.feature"))
    }
    .randomSwitch(
      20.0 -> exec(
        exec(session => session.markAsFailed)
          .exitHereIfFailed
      ),
      80.0 -> exec()
    )
    .group("shopProducts") {
      exec(karateFeature(name = "classpath:performance/feature/shopProducts.feature"))
        .doIfEqualsOrElse(session => session("isStaggered").as[String], "false"){
          exec(session => {
            session.set("jsonDesc", "{width:" + session("desNew").as[String].slice(0,3) + ", aspectRatio:" + session("desNew").as[String].slice(5,7) + "}")
          })
        }{
          exec(session => {
            session.set("jsonDesc", "{fwidth:" + session("desNew").as[String].slice(2,5) + ", faspect:" + session("desNew").as[String].slice(7,9) + ", rwidth:" + session("desNew").as[String].slice(21,24) + ", raspect:" + session("desNew").as[String].slice(26,28) + "}")
          })
        }

        // .exec(session => {
          // println(session)
          // session
        //})
    }
    .randomSwitch(
      20.0 -> exec(
        exec(session => session.markAsFailed)
          .exitHereIfFailed
      ),
      80.0 -> exec()
    )
    .group("findTire") {
      exec(karateFeature("classpath:performance/feature/findTire.feature"))
    }
    .randomSwitch(
      20.0 -> exec(
        exec(session => session.markAsFailed)
          .exitHereIfFailed
      ),
      80.0 -> exec()
    )
    .group("addToCart") {
      exec(karateFeature("classpath:performance/feature/addToCart.feature"))
    }


  setUp(
    // scn.inject(constantConcurrentUsers(10).during(3600 )).protocols(protocol)

      scn.inject(atOnceUsers( users = 1)).protocols(protocol)

  )

}